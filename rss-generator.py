import mechanize
from feedgen.feed import FeedGenerator
import urlparse
import requests
from bs4 import BeautifulSoup
import sys
import argparse
from argparse import RawTextHelpFormatter

DESC = """csf"""
ASCII = """ dnsj """


google_feed = (
    "GOOGLE SEARCH RESULTS",
    "htps://www.google.com",
    "Google search results for %s"
)


bing_feed = (
    "BING SEARCH RESULTS",
    "https://www.bing.com",
    "Bing search results for %s"
)


def generateFeed(urls, query, search_engine):

    # google
    if search_engine == 0:
        feed = google_feed
    elif search_engine == 1:
        feed = bing_feed

    fg = FeedGenerator()
    fg.title(feed[0])
    fg.link(href=feed[1], rel='alternate')
    fg.description(feed[2] % query)

    for url in urls:
        fe = fg.add_entry()
        fe.title(url[0])
        fe.link({'href': url[1], 'rel': 'alternate'})
        fe.description(url[2])

    print(fg.rss_str(pretty=True))
    # Write rss feed to file
    # fg.rss_file('rss.xml')


def get_results_page(query):
    br = mechanize.Browser()
    br.set_handle_robots(False)  # Google's robot.txt prevents from scrapping
    br.addheaders = [('User-agent', 'Mozilla/5.0')]
    status = br.open('http://www.google.com/')
    br.select_form(name='f')
    br.form['q'] = query
    return br.submit(), status.code

    
def get_bing_page(query):
    br = mechanize.Browser()
    br.set_handle_robots(False)  # Google's robot.txt prevents from scrapping
    br.addheaders = [('User-agent', 'Mozilla/5.0')]
    status = br.open('http://www.bing.com/search')
    formcount = 0
    for form in br.forms():
        if str(form.attrs["id"]) == "sb_form":
            break
        formcount += 1
    br.select_form(nr=formcount)
    br.form['q'] = query
    return br.submit(), status.code




def google_search(query):

    urls = []
    response, code = get_results_page(query)
    soup = BeautifulSoup(response.read(), 'lxml')
    for div in soup.find_all('div', class_='g'):
        desc = div.find_all('span', class_='st')
        try:
            anchor_tag = div.h3.a
        except:
            continue

        parsed_url = urlparse.urlparse(anchor_tag['href'])
        if 'url' in parsed_url.path:
            url_entry = [anchor_tag.text,
                         urlparse.parse_qs(parsed_url.query)['q'][0],
                         desc[0].text]
            urls.append(url_entry)
    return urls


def bing_search(query):

    urls = []
    response, code = get_bing_page(query)
    soup = BeautifulSoup(response.read(), 'lxml')

    for li in soup.findAll('li', attrs={'class': ['b_algo']}):
        title = li.h2.text.replace("\n", '').replace("  ", "")
        url = li.h2.a['href']
        desc = li.find('p').text
        url_entry = [title, url, desc]
        urls.append(url_entry)
    return urls

def main():
    parser = argparse.ArgumentParser(
        description='\n{}\n{}\n'.format(ASCII, DESC),
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument('--google', '-g', action='store_true',
                        help='Set search engine as Google')
    parser.add_argument('--bing', '-b', action='store_true',
                        help='Set search engine as Bing')
    parser.add_argument('-q', '--query', action='store', dest='query',
                        help='Specify search query. eg : --query "xkcd comics"'
                        )
    args = parser.parse_args()

    if args.google:
        if args.query:
            query = args.query
            urls = google_search(query)
        else:
            query = raw_input("Apakah yang ingin Anda cari? >> ")
            urls = google_search(query)
        generateFeed(urls, query, 0)

    elif args.bing:
        if args.query:
            query = args.query
            urls = bing_search(query)
        else:
            query = raw_input("Apakah yang ingin Anda cari? >> ")
            urls = bing_search(query)
        generateFeed(urls, query, 1)

    else:
        search_engine = int(raw_input(
            "Pilih search engine " +
            "(0 untuk google / 1 untuk bing ): "
        ))
        if search_engine not in [0, 1]:
            print("Pilihan Anda salah. Mohon pilih pilihan yang benar.")
            main()

        query = raw_input("Apakah yang ingin Anda cari? >> ")

        if search_engine == 0:
            # Google Search
            urls = google_search(query)
            generateFeed(urls, query, search_engine)

        elif search_engine == 1:
            # Bing Search
            urls = bing_search(query)
            generateFeed(urls, query, search_engine)

if __name__ == "__main__":
    main()
